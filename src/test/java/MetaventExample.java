import filter.FilteredStrings;
import filter.PowerOfTwoFilter;
import filter.StringFilter;
import me.foundry.metavents.EventManager;
import me.foundry.metavents.Listener;
import me.foundry.metavents.Priority;
import me.foundry.metavents.filter.Filtered;

/**
 * @author Foundry
 */
public class MetaventExample {
    private static final EventManager<Object> metaventManager = new EventManager<>();

    @Listener
    @Filtered(StringFilter.class)
    @FilteredStrings(value = {"foo", "bar"}, ignoreCase = true)
    private void onStringEvent(String event) {
        System.out.println(event);
    }

    @Listener
    @Filtered(PowerOfTwoFilter.class)
    protected static void onIntegerEvent(Integer event) {
        System.out.println(event);
    }

    @Listener(Priority.LOW)
    void onObjectEventLow(Object event) {
        System.out.println("Low Priority!");
    }

    @Listener
    void onObjectEventNormal(Object event) {
        System.out.println("Normal Priority!");
    }

    @Listener(Priority.HIGH)
    void onObjectEventHigh(Object event) {
        System.out.println("High Priority!");
    }

    public static void main(String[] args) {
        MetaventExample instance = new MetaventExample();
        metaventManager.subscribe(instance);

        metaventManager.post("Foo");
        metaventManager.post("Definitely Not Foo");
        metaventManager.post("BAR");
        metaventManager.post("-Definitely- Not Bar");

        for (int i = 0; i < 10; i++) {
            metaventManager.post(i);
        }

        metaventManager.post(new Object());

        metaventManager.unsubscribe(instance);

    }

}
