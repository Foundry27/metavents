package filter;

import me.foundry.metavents.ListenerElement;
import me.foundry.metavents.filter.Filter;

/**
 * @author Foundry
 */
public class PowerOfTwoFilter implements Filter<Integer> {
    @Override
    public boolean test(Integer event, ListenerElement<Integer> invoker) {
        return event % 2 == 0;
    }
}
