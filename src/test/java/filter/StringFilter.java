package filter;

import me.foundry.metavents.ListenerElement;
import me.foundry.metavents.filter.Filter;

/**
 * @author Foundry
 */
public class StringFilter implements Filter<String> {
    @Override
    public boolean test(String event, ListenerElement<String> invoker) {
        final FilteredStrings filteredStrings = invoker.getAnnotation(FilteredStrings.class);
        if (filteredStrings != null) {
            if (filteredStrings.ignoreCase()) {
                for (String string : filteredStrings.value()) {
                    if (event.equalsIgnoreCase(string)) return true;
                }
            } else {
                for (String string : filteredStrings.value()) {
                    if (event.equals(string)) return true;
                }
            }
        }
        return false;
    }
}
