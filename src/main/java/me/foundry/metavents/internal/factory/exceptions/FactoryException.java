package me.foundry.metavents.internal.factory.exceptions;

/**
 * @author Foundry
 */
public class FactoryException extends RuntimeException {
    public FactoryException(String message) {
        super(message);
    }
}
