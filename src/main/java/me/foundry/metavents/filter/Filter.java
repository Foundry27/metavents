package me.foundry.metavents.filter;

import me.foundry.metavents.ListenerElement;

import java.util.function.BiPredicate;

/**
 * @author Foundry
 */
@FunctionalInterface
public interface Filter<T> extends BiPredicate<T, ListenerElement<T>> {
    @Override
    boolean test(T event, ListenerElement<T> invoker);
}
