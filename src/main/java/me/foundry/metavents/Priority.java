package me.foundry.metavents;

/**
 * @author Foundry
 */
public enum Priority {
    HIGHEST, HIGHER, HIGH, NORMAL, LOW, LOWER, LOWEST
}
